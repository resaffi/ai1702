require 'json'
require 'open-uri'

(1..450).each do |page|
  url = 'https://newsapi.org/v2/everything?'\
    'q=lava-jato&'\
    "page=#{page}&"\
    'language=pt&'\
    'apiKey=ec79d040b3b5457da74f87466df2b0e8'
  req = open(url)
  response_body = req.read
  text = JSON.parse(response_body)
  articles = text['articles']
  File.open('final.txt', 'a') do |f|
    articles.each { |row| f.puts row['title'] }
  end
end


